<?php
header("Content-type: application/json; charset=UTF-8");
require 'models/participants.php';

//echo $_SERVER["REQUEST_URI"];
$parts = explode('/', $_SERVER['REQUEST_URI']);
if ($parts[2] != "participants") {
    http_response_code(404);
    exit;
}
$id = $parts[3] ?? null;
$request = $_SERVER['REQUEST_METHOD'];
switch ($request){
    case 'GET':
        if ($id) {
            Participant::find($id);
        } else {
            Participant::findAll();
        }
        break;
    case'POST':
        Participant::create();
        break;
    case 'PUT':
        Participant::update($id);
        break;
    case 'DELETE':
        Participant::delete($id);
        break;
}