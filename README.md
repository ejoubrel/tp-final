# TP final à rendre Dev Web

Serveur Apache & MySQL simple pour la gestion d'un évènement sportif à l'ENSAI


## Auteurs
- Elwenn Joubrel [@ejoubrel](https://gitlab.com/ejoubrel)
- Lorenzo Mathieu [@Stenaxo](https://github.com/Stenaxo)


## Usage
La page dispose de 4 onglets :
- **Accueil** présente l'évènement
- **Inscription** permet de s'inscrire à la compétition
- **Participants** affiche la liste des participants, et si besoin leurs détails, en utilisant Ajax
- **Participants (API)** réalise la même action, en utilisant une application vue.js

## Installation

- Installer XAMPP et lancer les services Apache et MySQL,
- Cloner ce repository dans le dossier `htdocs` de XAMPP (`/opt/lampp/htdocs` sur Ubuntu), ou y placer ce directory,
- Dans un navigateur, ouvrir `localhost/phpmyadmin/` pour accéder au service MySQL et gérer les bases de données,
- Créer une nouvelle base nommée `tp_final`, et y éxecuter le script SQL suivant pour créer et remplir la table :

```sql
CREATE TABLE participants (
id INTEGER NOT NULL AUTO_INCREMENT,
login VARCHAR(50) NOT NULL UNIQUE,
password VARCHAR(50) NOT NULL,
prenom VARCHAR(50) NOT NULL,
sexe VARCHAR(20),
nom VARCHAR(50) NOT NULL,
telephone VARCHAR(50),
naissance VARCHAR(50),
souscription VARCHAR(50),
PRIMARY KEY (id)
);

INSERT INTO `participants` (`id` , `login`, `password`, `prenom`, `sexe` ,`nom` ,`telephone` ,`naissance`, `souscription` ) VALUES (NULL, 'jrault', '908UCZ', 'Jean', 'M','Rault', '0223456794', '27/05/1996', '14/09/2023');
INSERT INTO `participants` (`id` , `login`, `password`, `prenom`, `sexe` ,`nom` ,`telephone` ,`naissance`, `souscription` ) VALUES (NULL, 'mjohnson', '259MTR', 'Melvin', 'M', 'Johnson', '0223097452', '06/03/1997', '21/09/2023');
INSERT INTO `participants` (`id` , `login`, `password`, `prenom`, `sexe` ,`nom` ,`telephone` ,`naissance`, `souscription` ) VALUES (NULL, 'jbidault', '159PWZ', 'Jerome', 'M', 'Bidault', '0299129564', '12/08/1996', '22/09/2023');
INSERT INTO `participants` (`id` , `login`, `password`, `prenom`, `sexe` ,`nom` ,`telephone` ,`naissance`, `souscription` ) VALUES (NULL, 'lnicolas', '173VTR', 'Lucie', 'F', 'Nicolas', '0299129450', '29/06/1997', '06/10/2023');
INSERT INTO `participants` (`id` , `login`, `password`, `prenom`, `sexe` ,`nom` ,`telephone` ,`naissance`, `souscription` ) VALUES (NULL, 'cbaillet', '850YIA', 'Christine', 'F', 'Baillet', '0299123177', '24/08/1997', '08/10/2023');
```

- Accéder à `localhost/tp_final` puis naviguer le site pour accéder aux différents services.