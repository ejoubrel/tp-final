<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Participants</title>
    <link rel="stylesheet" href="style.css">
</head>
<script>
function displayParticipant(str) {
    if (str==""){
        document.getElementbyId("descParticipant").innerHTML="";
        return;
    }
    xhr = new XMLHttpRequest();
    xhr.onreadystatechange=function() {
        if (xhr.readyState == 4 && xhr.status == 200) {
            document.getElementById("descParticipant").innerHTML = xhr.responseText;
        }
    }
    xhr.open("GET", "getParticipant.php?q="+str, true);
    xhr.send()
}
</script>
<body>
<div class="box">
    <div class="topnav">
        <a href="index.html">Accueil</a>
        <a href="inscription.html">Inscription</a>
        <a class="active" href="listParticipants.php">Participants</a>
        <a href="listParticipantsVue.html">Participants (API)</a>
    </div>
    <div class=content>
        <div class="container">
            <div class="wrap">
                <div class="fleft">
                    <h2>Liste des participants</h2>
                    <?php
                    try {
                        $bdd = new PDO('mysql:host=localhost;dbname=tp_final', 'root', '');
                    }
                    catch (Exception $e) {
                        die('Erreur : ' . $e->getMessage());
                    }

                    $res = $bdd->query("SELECT * FROM participants") or die(print($bdd->errorInfo()));

                    if ($res->rowCount() > 0) {
                        while ($data = $res->fetch()) {
                            $idParticipant = $data["id"];
                            echo "<a class='clickName' id='{$idParticipant}' href='#' onclick='displayParticipant(this.id)'>{$data['prenom']} {$data['nom']}</a><br/>";
                        };
                    } else {
                        echo "O results";
                    }
                    ?>
                </div>
                <div class="fright">
                    <h2>Détails du participant :</h2>
                    <div id="descParticipant"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer">
        <p class="info">© 2024 Compétition Sportive Ensai. Tous droits réservés.</p>
    </div>
</div>
</body>
</html>