<?php
require_once 'db.php';

class Participant {
    public static function create(){
        $participant = json_decode(file_get_contents('php://input'),true);
        global $connection;

        $login = $participant["login"];
        $password = $participant["password"];
        $prenom = $participant["prenom"];
        $sexe = $participant["sexe"];
        $nom = $participant["nom"];
        $telephone = $participant["telephone"];
        $naissance = $participant["naissance"];
        $souscription = date("d/m/Y");

        $sql = "INSERT INTO participants (login, password, prenom, sexe, nom, telephone, naissance, souscription) VALUES ('$login', '$password', '$prenom', '$sexe', '$nom', '$telephone', '$naissance', '$souscription')";

        if ($connection->query($sql) === TRUE) {
            header('HTTP/1.1 200 OK');
        } else {
            header('HTTP/1.1 200 OK');
        }
    }

    public static function findAll() {
        global $connection;
        $query  = 'SELECT * FROM participants';
        $stmt = $connection->prepare($query );
        $stmt->execute();
        $participants = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if (count($participants) > 0) {
          header('HTTP/1.1 200 OK');
          echo json_encode($participants);
        } else {
          header('HTTP/1.1 500 Internal Server Error');
        }
        return $participants;
    }

    public static function find($id) {
        global $connection;
        $query  = 'SELECT * FROM participants WHERE id = ?';
        $stmt = $connection->prepare($query );
        $stmt->bindParam(1, $id);
        $stmt->execute();
        $participants = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if (count($participants) > 0) {
          header('HTTP/1.1 200 OK');
          echo json_encode($participants);
        } else {
          header('HTTP/1.1 404 Object not found');
        }
        return $participants;
    }
    public static function delete($id) {
      global $connection;
      $query  = 'DELETE FROM participants WHERE id = ?';
      $stmt = $connection->prepare($query);
      $stmt->bindParam(1, $id);
      $result = $stmt->execute();
      if ($result) {
        header('HTTP/1.1 200 OK');
      } else {
        header('HTTP/1.1 500 Internal Server Error');
      }
    }
  
    public static function update($id) {
      $todo = json_decode(file_get_contents('php://input'), true);
      global $connection;
      $query  = 'UPDATE participants SET title = ?, content = ?, is_completed = ? WHERE id = ?';
      $stmt = $connection->prepare($query);
      $stmt->bindParam(1, $todo['title'], PDO::PARAM_STR);
      $stmt->bindParam(2, $todo['content'], PDO::PARAM_STR);
      $stmt->bindParam(3, $todo['is_completed'], PDO::PARAM_INT);
      $stmt->bindParam(4, $id, PDO::PARAM_INT);
  
      $result = $stmt->execute();
      if ($result) {
        header('HTTP/1.1 200 OK');
      } else {
        header('HTTP/1.1 500 Internal Server Error');
      }
    }
}