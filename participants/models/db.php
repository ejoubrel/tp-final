<?php
class Db {
    public function connect() {
        try {
            $conn = new PDO("mysql:host=localhost;dbname=tp_final","root","");
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $conn->exec("SET CHARACTER SET utf8");
    } catch (PDOException $e) {
        echo 'Connection Error: ' . $e->getMessage();
    }
    return $conn;
  }
}

$db = new Db();
$connection = $db->connect();
if (!$connection) {
  die('Connection error');
}